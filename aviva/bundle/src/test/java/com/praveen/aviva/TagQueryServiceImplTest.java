package com.praveen.aviva;


import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.praveen.aviva.impl.TagQueryServiceImpl;

import io.wcm.testing.mock.aem.junit.AemContext;
import junit.framework.Assert;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import javax.jcr.Session;
import java.util.*;

/**
 * One implementation of the {@link TagQueryService}.
 */
@PrepareForTest(TagQueryServiceImpl.class)
public class TagQueryServiceImplTest{

    @Rule
    public final AemContext context = new AemContext();

    @Test
    public void should_handle_null_tags() {

        List<String> tagList = null;
        String operation = null;

        List<String> emptyList = new ArrayList<String>();

        TagQueryServiceImpl tagService = new TagQueryServiceImpl();
        List<String> returnList = tagService.getPagesTagged(tagList, operation);
        Assert.assertEquals(returnList, emptyList);

    }

    @Test
    public void should_handle_empty_tags_list() {


        String operation = null;
        List<String> emptyList = new ArrayList<String>();

        TagQueryServiceImpl tagService = new TagQueryServiceImpl();
        List<String> returnList = tagService.getPagesTagged(emptyList, operation);
        Assert.assertEquals(returnList, emptyList);

    }



    public void testResultFromQuery() throws Exception{


        final ResourceResolver resourceResolver = PowerMockito.spy(context.resourceResolver());
        QueryBuilder queryBuilder = Mockito.mock(QueryBuilder.class);
        Session session = Mockito.mock(Session.class);
        Mockito.doReturn(session).when(resourceResolver).adaptTo(Session.class);
        Query query = Mockito.mock(Query.class);
        SearchResult searchResult = Mockito.mock(SearchResult.class);

        java.util.List<Hit> hits = new ArrayList<Hit>() {{
            add(getMockedHit(context.resourceResolver().getResource("/content/geometrixx-outdoors/en/brand")));
            add(getMockedHit(context.resourceResolver().getResource("/content/geometrixx-outdoors/en/women.html")));
            add(getMockedHit(context.resourceResolver().getResource("/content/geometrixx-outdoors/en/men.html")));
        }};

        Mockito.when(queryBuilder.createQuery(PredicateGroup.create(Mockito.anyMap()),Mockito.any(Session.class))).thenReturn(query);
        Mockito.when(query.getResult()).thenReturn(searchResult);
        Mockito.when(searchResult.getHits()).thenReturn(hits);

    }


    private Hit getMockedHit(Resource resource) throws Exception {
        Hit hit = Mockito.mock(Hit.class);
        Mockito.when(hit.getResource()).thenReturn(resource);
        return hit;
    }


}