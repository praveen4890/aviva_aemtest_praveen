package com.praveen.aviva;

import java.util.List;

/**
 * A simple service interface
 */
public interface TagQueryService {
    
    /**
     * @return the name of the underlying implementation
     */
    public List<String> getPagesTagged(List<String> tagList,String queryOperation);

}