package com.praveen.aviva.taginfo;

import com.adobe.cq.sightly.WCMUse;
import com.praveen.aviva.TagQueryService;
import org.apache.jackrabbit.oak.commons.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by praveen on 16/6/16.
 */
public class TagInfo extends WCMUse{

    private Logger logger = LoggerFactory.getLogger(TagInfo.class);

    private List<String> tagList;
    private String operation;
    private List<String> result;

    private TagQueryService tagQueryService;

    @Override
    public void activate(){
        String[] temp = getProperties().get("tags",String[].class);
        this.tagList = Arrays.asList(temp);
        this.operation = getProperties().get("operation",String.class);

        if(this.operation == null)
            this.operation = "false";

        this.tagQueryService = getSlingScriptHelper().getService(TagQueryService.class);
        logger.info("Inside activate() : tags: "+tagList+" operation: "+operation);
    }

    public List<String> getResult(){
        logger.info("Calling the service method...");
        this.result =  tagQueryService.getPagesTagged(tagList,operation);
        return result;
    }

}
