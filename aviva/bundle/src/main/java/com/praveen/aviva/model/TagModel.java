package com.praveen.aviva.model;

import com.praveen.aviva.TagQueryService;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * Created by praveen on 17/6/16.
 */
@Model(adaptables={Resource.class})
public class TagModel {

    @Inject
    private String[] tags;

    @Inject @Default(values = "false")
    private String operation;

    @Inject @Source("osgi-services")
    private TagQueryService tagQueryService;

    private List<String> results;

    @PostConstruct
    public void activate() {

        results = tagQueryService.getPagesTagged(Arrays.asList(tags),operation);
    }

    public List<String> getResults(){
        return results;
    }
}
