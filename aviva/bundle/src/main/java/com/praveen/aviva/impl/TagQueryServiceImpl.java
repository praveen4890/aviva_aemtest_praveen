package com.praveen.aviva.impl;

import javax.jcr.Repository;
import javax.jcr.Session;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import com.praveen.aviva.TagQueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * One implementation of the {@link TagQueryService}.
 */
@Service
@Component(metatype = false)
public class TagQueryServiceImpl implements TagQueryService {
    
    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private QueryBuilder queryBuilder;

    private Session session;
    private Logger logger = LoggerFactory.getLogger(TagQueryServiceImpl.class);


    public List<String> getPagesTagged(List<String> tagList,String queryOperation){
        List<String> resultList = new ArrayList<String>();

        try{

            // Handle empty input list
            if(tagList == null || tagList.size() == 0)
                return resultList;

            // Get session object
            ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            session = resourceResolver.adaptTo(Session.class);

            // Build the query map
            Map<String,String> map = new HashMap<String, String>();

            // Set the search path
            map.put("path","/content");
            map.put("type","cq:Page");

            // Set limit to full
            map.put("p.limit","-1");

            // Add the Tag predicates
            map.put("property","jcr:content/cq:tags");
            map.put("property.and",queryOperation);

            for(int i=0;i<tagList.size();i++)
                map.put("property."+(i+1)+"_value",tagList.get(i));

            // TO DO - set predicate for last modified date
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -5);

            map.put("daterange.property","jcr:content/cq:lastModified");
            map.put("daterange.lowerBound",dateFormat.format(cal.getTime()));
            map.put("daterange.lowerOperation",">=");


            // Build query
            Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
            query.setStart(0);

            SearchResult results = query.getResult();

            // Iterate the result object and prepare list
            for(Hit hit : results.getHits()){
                resultList.add(hit.getPath());
            }

            // close the session
            session.logout();
            return resultList;

        }catch (Exception e){
            logger.info("Error while fetching the query results"+ e.getMessage());

        }
        return null;
    }

}
